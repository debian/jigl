#!/bin/csh -f
#############
# Author: Jason Paul
# Date:   July 31, 2001
#
# rotate - take an image file, extract the exif information from it, rotate
# the image and then put back the exif information. This is useful for
# rotating images shot with digital cameras. Some programs do not retain
# the exif info so when you rotate it you lose the info about the shot.
#
# rotate [[-f|-flip|--flip] [horizontal | vertical]"
#         [-r|-rotate|--rotate] [90 | 180 | 270] "
#         [-tp|-transpose|--transpose]"
#         [-tv|-transverse|--transverse]]"
#         files"
#
# Options:
# -------
# -f|-flip|--flip [horizontal | vertical]
#  horizontal: Mirror image horizontally (left-right).
#  vertical  : Mirror image vertically (top-bottom).
#
# -r|-rotate|--rotate [90 | 180 | 270]
#  90 : Rotate image 90 degrees clockwise.
#  180: Rotate image 180 degrees.
#  270: Rotate image 270 degrees clockwise (or 90 ccw).
#
# -tp|-transpose|--transpose
#  Transpose image (across UL-to-LR axis).
#
# -tv|-transverse|--transverse
#  Transverse transpose (across UR-to-LL axis).
#
# files
#  The files you want to rotate. Wildcards are acceptable.
#  The original files will be overwritten
#
## Requirements: 
# ------------
# - libjpeg is needed for the 'jpegtran' program needed to rotate the
#   images. This does lossless rotation on jpeg images.
# - jhead is needed to extract the exif info from the file
#
# History:
# 7/31/2001 - 1.0 - initial version
# 2/9/2002  - 1.1 - increased error handling on bad or no input
#                 - increased number of ways to call each argument
#
#############

set version = 1.1

# check to see if any arguments were given
if ($#argv < 1) then
    echo "no arguments given to rotate"
    set firstArg = "--help"
else
    set firstArg = $argv[1]
endif

# this whole switch statement breaks apart the command line args to 
# rotate so they can be used with the jhead and jpegtran programs
#
switch ($firstArg)
    case -v:
    case -version:
    case --version:
        echo "rotate version $version"
        exit
    case -f:
    case -flip:
    case --flip:
        if ($#argv > 1) then 
            switch ($argv[2])
                case horizontal:
                    # don't do anything, just make sure the arg is valid
                    breaksw
                case vertical:
                    # don't do anything, just make sure the arg is valid
                    breaksw
                default:
                    echo "Invalid argument $argv[2] to -flip"
                    echo "Possible values are:"
                    echo "-flip [horizontal | vertical]"
                    exit
            endsw
        else
            echo "Not enough arguments to -flip"
            echo "-flip [horizontal | vertical]"
            exit
        endif
        set cmdStr = "-flip $argv[2]"
        set fileStr = "$argv[3-]"
        breaksw
    case -r:
    case -rotate:
    case --rotate:
        if ($#argv > 1) then 
            switch ($argv[2])
                case "90":
                    # don't do anything, just make sure the arg is valid
                    breaksw
                case "180":
                    # don't do anything, just make sure the arg is valid
                    breaksw
                case "270":
                    # don't do anything, just make sure the arg is valid
                    breaksw
                default:
                    echo "Invalid argument $argv[2] to -rotate"
                    echo "possible values are:"
                    echo "-rotate [90 | 180 | 270] "
                    exit
            endsw
        else 
            echo "Not enough arguments to -rotate"
            echo "-rotate [90 | 180 | 270] "
            exit
        endif
        set cmdStr = "-rotate $argv[2]"
        set fileStr = "$argv[3-]"
        breaksw
    case -tp:
    case -transpose:
    case --transpose:
        set cmdStr = "-transpose"
        set fileStr = "$argv[2-]"
        breaksw
    case -tv:
    case -transverse:
    case --transverse:
        set cmdStr = "-transverse"
        set fileStr = "$argv[2-]"
        breaksw
    default:
        echo "--------------------------------------"
        echo " rotate [[-f|-flip|--flip] [horizontal | vertical]"
        echo "         [-r|-rotate|--rotate] [90 | 180 | 270] "
        echo "         [-tp|-transpose|--transpose]"
        echo "         [-tv|-transverse|--transverse]]"
        echo "         files"
        echo ""
        echo " Options:"
        echo " -------"
        echo " [-f|-flip|--flip] [horizontal | vertical]"
        echo "  horizontal: Mirror image horizontally (left-right)."
        echo "  vertical  : Mirror image vertically (top-bottom)."
        echo ""
        echo " [-r|-rotate|--rotate] [90 | 180 | 270]"
        echo "  90 : Rotate image 90 degrees clockwise."
        echo "  180: Rotate image 180 degrees."
        echo "  270: Rotate image 270 degrees clockwise (or 90 ccw)."
        echo ""
        echo " [-tp|-transpose|--transpose]"
        echo "  Transpose image (across UL-to-LR axis)."
        echo ""
        echo " [-tv|-transverse|--transverse]"
        echo "  Transverse transpose (across UR-to-LL axis)."
        echo ""
        echo " files"
        echo "  The files you want to rotate. Wildcards are acceptable."
        echo "--------------------------------------"
        echo ""
    exit
endsw

if ($fileStr == "") then
    echo "No files were specified to process"
    exit
endif
#echo "command string to jpegtrans: $cmdStr"
#echo "file string to jpegtrans   : $fileStr"

jhead -cmd "jpegtran $cmdStr -outfile &o &i" $fileStr
